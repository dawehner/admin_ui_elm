module App exposing (..)

import Html exposing (Html, div, em, img, input, span, table, tbody, td, text, th, thead, tr)
import Html.Attributes exposing (checked, disabled, placeholder, src, type_, value, colspan)
import Html.Events exposing (onClick, onInput)
import Html.Lazy exposing (lazy)
import Http
import Json.Decode as JD
import List
import List.Extra
import RemoteData
import String
import Dict exposing (Dict)


---- MODEL ----


type alias Model =
    { roles : RemoteData.WebData (List Role)
    , permissions : RemoteData.WebData (List Permission)
    , filterString : Maybe String
    , modules : RemoteData.WebData (Dict String String)
    }


type alias PermissionId =
    String


type alias Permission =
    { id : PermissionId
    , description : String
    , provider : String
    , title : String
    , restrictAccess : Bool
    }


type alias RoleId =
    String


type alias Role =
    { id : RoleId
    , label : String
    , permissions : List PermissionId
    , isAdmin : Bool
    }


init : ( Model, Cmd Msg )
init =
    ( { roles = RemoteData.Loading, permissions = RemoteData.Loading, modules = RemoteData.Loading, filterString = Nothing }, getPermissionsAndRoles )



---- UPDATE ----


type Msg
    = TogglePermission PermissionId RoleId
    | GetPermissionsAndRolesResult (RemoteData.WebData ( List Role, List Permission, Dict String String ))
    | FilterPage String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TogglePermission permissionId roleId ->
            let
                roles_ =
                    RemoteData.map
                        (List.Extra.updateIf (.id >> (==) roleId)
                            (\role ->
                                { role
                                    | permissions =
                                        if List.member permissionId role.permissions then
                                            List.Extra.remove permissionId role.permissions
                                        else
                                            permissionId :: role.permissions
                                }
                            )
                        )
                        model.roles
            in
                ( { model | roles = roles_ }, Cmd.none )

        GetPermissionsAndRolesResult (RemoteData.Success ( roles, permissions, modules )) ->
            ( { model | roles = RemoteData.succeed roles, permissions = RemoteData.succeed permissions, modules = RemoteData.succeed modules }, Cmd.none )

        GetPermissionsAndRolesResult (RemoteData.NotAsked) ->
            ( { model | roles = RemoteData.NotAsked, permissions = RemoteData.NotAsked }, Cmd.none )

        GetPermissionsAndRolesResult (RemoteData.Loading) ->
            ( { model | roles = RemoteData.Loading, permissions = RemoteData.Loading }, Cmd.none )

        GetPermissionsAndRolesResult (RemoteData.Failure e) ->
            ( { model | roles = RemoteData.Failure e, permissions = RemoteData.Failure e }, Cmd.none )

        FilterPage string ->
            ( { model
                | filterString =
                    if string == "" then
                        Nothing
                    else
                        Just string
              }
            , Cmd.none
            )


getPermissionsAndRoles : Cmd Msg
getPermissionsAndRoles =
    Http.get "http://localhost/d8/elm/roles" decodePermissionsAndRoles
        |> RemoteData.sendRequest
        |> Cmd.map GetPermissionsAndRolesResult


decodePermissions =
    JD.list decodePermission


decodePermission =
    JD.map5 Permission
        (JD.field "id" JD.string)
        (JD.field "description" (JD.map (Maybe.withDefault "") (JD.maybe JD.string)))
        (JD.field "provider" JD.string)
        (JD.field "title" JD.string)
        (JD.map (Maybe.withDefault False)
            (JD.maybe (JD.field "restrict access" JD.bool))
        )


decodeRoles =
    JD.list decodeRole


decodeRole =
    JD.map4 Role
        (JD.field "id" JD.string)
        (JD.field "label" JD.string)
        (JD.field "permissions" (JD.list JD.string))
        (JD.field "is_admin" JD.bool)


decodeModuleNames : JD.Decoder (Dict String String)
decodeModuleNames =
    JD.map Dict.fromList
        (JD.list
            (JD.map2 (,)
                (JD.field "provider" JD.string)
                (JD.field "providerHumanName" JD.string)
            )
        )


decodePermissionsAndRoles : JD.Decoder ( List Role, List Permission, Dict String String )
decodePermissionsAndRoles =
    JD.map3 (,,)
        (JD.field "roles" decodeRoles)
        (JD.field "permissions" decodePermissions)
        (JD.field "permissions" decodeModuleNames)



---- VIEW ----


merge3 :
    RemoteData.RemoteData e a
    -> RemoteData.RemoteData e b
    -> RemoteData.RemoteData e c
    -> RemoteData.RemoteData e ( a, b, c )
merge3 a b c =
    RemoteData.map (,,) a
        |> RemoteData.andMap b
        |> RemoteData.andMap c


view : Model -> Html Msg
view model =
    let
        permissionsAndRoles =
            merge3 model.permissions model.roles model.modules
    in
        case permissionsAndRoles of
            RemoteData.Success ( permissions, roles, modules ) ->
                viewApp model.filterString permissions roles modules

            RemoteData.Failure _ ->
                text "failure"

            RemoteData.Loading ->
                text "Loading ..."

            RemoteData.NotAsked ->
                text "Not asked ..."


viewApp : Maybe String -> List Permission -> List Role -> Dict String String -> Html Msg
viewApp filterString permissions roles modules =
    div []
        [ searchBox filterString FilterPage
        , viewPermissionMatrix (filterPermissions filterString permissions) (filterRoles filterString roles) modules
        ]


filterPermissions : Maybe String -> List Permission -> List Permission
filterPermissions filterString permissions =
    case filterString of
        Nothing ->
            permissions

        Just string ->
            let
                permissions_ =
                    List.filter
                        (\perm ->
                            String.contains string
                                (perm.title
                                    --++ Maybe.withDefault "" perm.description
                                    ++
                                        perm.provider
                                )
                        )
                        permissions
            in
                if List.length permissions_ == 0 then
                    permissions
                else
                    permissions_


filterRoles : Maybe String -> List Role -> List Role
filterRoles filterString roles =
    case filterString of
        Nothing ->
            roles

        Just string ->
            let
                roles_ =
                    List.filter
                        (\role ->
                            String.contains string (role.id ++ role.label)
                        )
                        roles
            in
                -- In case no roles where found, display all
                if List.length roles_ == 0 then
                    roles
                else
                    roles_


roleHasPermission permissionId role =
    List.member permissionId role.permissions


searchBox filterString msg =
    input [ type_ "textfield", placeholder "Search ...", value (Maybe.withDefault "" filterString), onInput msg ] []


checkedCheckbox msg =
    input [ type_ "checkbox", checked True, onClick msg ] []


uncheckedCheckbox msg =
    input [ type_ "checkbox", checked False, onClick msg ] []


disabledCheckbox msg =
    input [ type_ "checkbox", checked True, disabled True ] []


viewPermissionTitle : Permission -> List (Html Msg)
viewPermissionTitle permission =
    let
        title =
            text permission.title

        description =
            div [] [ text permission.description ]

        restrictAccess =
            if permission.restrictAccess then
                div [] [ em [] [ text "Warning: Give to trusted roles only; this permission has security implications.\n  " ] ]
            else
                span [] []
    in
        [ title, restrictAccess ]


groupBy : (a -> comparable) -> List a -> Dict comparable (List a)
groupBy keyfn list =
    let
        reducer elem dict =
            let
                key =
                    keyfn elem

                addTo values =
                    case values of
                        Nothing ->
                            Just [ elem ]

                        Just old ->
                            Just (elem :: old)
            in
                Dict.update key addTo dict
    in
        List.foldl reducer Dict.empty list


flatten2D : List (List a) -> List a
flatten2D list =
    List.foldr (++) [] list


groupPermissionsByModule : List Permission -> Dict String (List Permission)
groupPermissionsByModule =
    groupBy (.provider)


viewPermissionMatrix permissions roles modules =
    let
        groups =
            groupPermissionsByModule permissions
    in
        table []
            [ thead [] <|
                th [] [ text "Permission" ]
                    :: List.map (\role -> th [] [ text role.label ]) roles
            , Dict.map
                (\provider permissions ->
                    tr []
                        [ td [ colspan (List.length roles + 1) ] [ text <| Maybe.withDefault provider <| Dict.get provider modules ]
                        ]
                        :: List.map
                            (lazy
                                (\permission -> viewPermissionRow permission roles)
                            )
                            permissions
                )
                groups
                |> Dict.values
                |> flatten2D
                |> tbody []
            ]


viewPermissionRow permission roles =
    tr [] <|
        td [] (viewPermissionTitle permission)
            :: List.map
                (\role ->
                    let
                        checkbox =
                            if role.isAdmin then
                                disabledCheckbox
                            else if roleHasPermission permission.id role then
                                checkedCheckbox
                            else
                                uncheckedCheckbox
                    in
                        td [] [ checkbox <| TogglePermission permission.id role.id ]
                )
                roles



---- PROGRAM ----


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        }
