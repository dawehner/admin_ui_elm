module View exposing (view)

import Html exposing (Html, text, div, img, ul, li, details, summary, p, input, a, h2)
import Html.Attributes exposing (src, type_, disabled, checked, placeholder, value, attribute, class, href)
import Html.Events exposing (onClick, onInput)
import Model exposing (..)
import RemoteData
import Maybe.Extra
import Dict
import Dict.Extra


open =
    attribute "open"


role =
    attribute "role"


view : Model -> Html Msg
view model =
    div []
        [ viewMessages model.messages
        , viewModulesEnableQueue model.modulesEnableQueue
        , input [ type_ "textfield", placeholder "Filter by name or description", value model.nameFilter, onInput ChangeNameFilter ] []
        , viewRemoteData (\modules -> viewModuleGroups (filterModulesByName model.nameFilter modules)) model.modules
        ]


viewModulesEnableQueue : List String -> Html Msg
viewModulesEnableQueue modules =
    if List.length modules > 0 then
        div []
            [ h2 [] [ text "Modules enabling ..." ]
            , List.map (\name -> li [] [ text name ]) modules
                |> ul []
            ]
    else
        text ""


viewRemoteData : (a -> Html Msg) -> RemoteData.RemoteData e a -> Html Msg
viewRemoteData render remoteData =
    case remoteData of
        RemoteData.NotAsked ->
            text "Not asked ..."

        RemoteData.Loading ->
            text "Loading ..."

        RemoteData.Success data ->
            render data

        RemoteData.Failure _ ->
            text "error"


viewMessages : List String -> Html Msg
viewMessages messages =
    List.map (text >> List.singleton >> li []) messages
        |> ul []
        |> List.singleton
        |> div [ role "contentinfo" ]


viewModuleGroups : List Module -> Html Msg
viewModuleGroups modules =
    let
        groups =
            Dict.Extra.groupBy (\mod -> Maybe.withDefault "Other" mod.package) modules
    in
        Dict.map
            (\k modules -> details [ open "" ] [ summary [] [ text k ], viewModuleGroup modules ])
            groups
            |> Dict.values
            |> div []


viewModuleGroup : List Module -> Html Msg
viewModuleGroup modules =
    List.map viewModule modules
        |> ul []


filterModulesByName : String -> List Module -> List Module
filterModulesByName string modules =
    List.filter (\mod -> String.contains string (mod.humanName ++ mod.name ++ (Maybe.withDefault "" mod.description))) modules


disabledCheckbox : Html Msg
disabledCheckbox =
    input [ type_ "checkbox", disabled True, checked True ] []


checkbox : Msg -> Html Msg
checkbox msg =
    input [ type_ "checkbox", onClick msg ] []


viewModule : Module -> Html Msg
viewModule mod =
    li []
        [ div []
            [ text mod.humanName
            , if mod.enabled then
                disabledCheckbox
              else
                checkbox (ModuleEnable mod.name)
            , details []
                [ summary [] [ text <| Maybe.withDefault "no descriptipion" mod.description ]
                , p []
                    [ text ("Machine name: " ++ mod.name)
                    ]
                , if List.length mod.dependencies > 0 then
                    p []
                        -- Use human readable names ...
                        [ text ("Requires: " ++ (String.join ", " mod.dependencies))
                        ]
                  else
                    text "No dependencies"
                ]
            ]
        ]


moduleLinks : Module -> Html Msg
moduleLinks mod =
    Maybe.Extra.values
        [ Maybe.map (\url -> a [ href url, class "module-link-help" ] [ text "help" ]) mod.helpUrl
        , Maybe.map (\url -> a [ href url, class "module-link-permission" ] [ text "Permissions" ]) mod.permissionUrl
        , Maybe.map (\url -> a [ href url, class "module-link-configure" ] [ text "Configure" ]) mod.configUrl
        ]
        |> div [ class "links" ]
