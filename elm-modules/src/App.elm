module App exposing (..)

import RemoteData
import Model exposing (..)
import Update exposing (..)
import View exposing (view)
import Html


init =
    ( { modules = RemoteData.Loading, nameFilter = "", messages = [], modulesEnableQueue = [] }, getModules )


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        }
