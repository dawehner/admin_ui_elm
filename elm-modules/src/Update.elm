module Update exposing (..)

import Json.Decode as JD
import Json.Decode.Pipeline as JDP
import Http
import RemoteData
import Model exposing (..)


decodeGetModules : JD.Decoder (List Module)
decodeGetModules =
    JD.list decodeModule


decodeModule : JD.Decoder Module
decodeModule =
    JDP.decode Module
        |> JDP.required "name" JD.string
        |> JDP.required "human_name" JD.string
        |> JDP.required "description" (JD.maybe JD.string)
        |> JDP.required "package" (JD.maybe JD.string)
        |> JDP.required "dependencies" (JD.list (JD.string))
        |> JDP.required "status" JD.bool
        |> JDP.required "helpUrl" (JD.maybe JD.string)
        |> JDP.required "configUrl" (JD.maybe JD.string)
        |> JDP.required "permissionUrl" (JD.maybe JD.string)


getModules : Cmd Msg
getModules =
    let
        url =
            "http://localhost/d8/elm/modules"

        request =
            Http.get url decodeGetModules
    in
        RemoteData.sendRequest request
            |> Cmd.map ModulesLoadingResult


enableModule : String -> Cmd Msg
enableModule name =
    let
        url =
            "http://localhost/d8/elm/modules/enable/" ++ name

        request =
            Http.post url Http.emptyBody decodeEnableModule
    in
        RemoteData.sendRequest request
            |> Cmd.map ModulesEnableResult


decodeEnableModule : JD.Decoder String
decodeEnableModule =
    JD.string


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ModulesLoadingResult result ->
            ( { model | modules = result }, Cmd.none )

        ModuleEnable name ->
            -- Use RemoteData ...
            ( { model | modulesEnableQueue = name :: model.modulesEnableQueue }, enableModule name )

        -- Implement this ... :) ---
        ModulesEnableResult (RemoteData.Success result) ->
            ( { model
                | modulesEnableQueue = List.filter ((/=) result) model.modulesEnableQueue
                , messages = ("Module " ++ result ++ " enabled") :: model.messages
              }
            , Cmd.none
            )

        ModulesEnableResult _ ->
            ( { model | messages = [ "meh" ] }, Cmd.none )

        ChangeNameFilter string ->
            ( { model | nameFilter = string }, Cmd.none )
