# Elm

[Elm](http://elm-lang.org/) is delightful language to build web apps. Some advantages over JS:

* No runtime exceptions
* Great performance by default
* Flexible type system
* Incredible good compiler messages

# Pages

* ```/admin/modules-elm```
* ```/admin/people/permissions-elm```
* ```/admin/reports/dblog-elm```

# Build

* ```make build```

# Develop

Executing any of these commands should open 

* ```make dev-modules```
* ```make dev-permissions```
* ```make dev-dblog```

# Contribute

Please get involved:

* Create issues
* Chat in the #javascript Drupal slack room
* Try it out
* 
