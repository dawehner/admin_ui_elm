<?php

namespace Drupal\admin_ui_elm\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RenderContext;
use Drupal\user\Entity\Role;

class Controller extends ControllerBase {

  public function permissionsPage() {
    $build['#attached']['library'][] = 'admin_ui_elm/permissions';
    $build['#markup'] = '<div id="root" />';
    return $build;
  }

  public function rolesWithPermissions() {
    /** @var \Drupal\Core\Render\RendererInterface $render */
    $render = \Drupal::service('renderer');
    $module_handler = \Drupal::moduleHandler();
    

    $context = new RenderContext();
    $response = $render->executeInRenderContext($context, function () use ($module_handler) {
      /** @var \Drupal\user\PermissionHandlerInterface $permission_handler */
      $permission_handler = \Drupal::service('user.permissions');
      $permissions = $permission_handler->getPermissions();
      foreach ($permissions as $id => $permission) {
        $permissions[$id]['id'] = $id;
        $permissions[$id]['providerHumanName'] = $module_handler->getName($permission['provider']);
      }
      /** @var \Symfony\Component\Serializer\Serializer $normalizer */
      $normalizer = \Drupal::service('serializer');
      $roles = Role::loadMultiple();

      $data = [
        'roles' => array_values($normalizer->normalize($roles, 'json')),
        'permissions' => array_values($permissions),
      ];
      $response = CacheableJsonResponse::create($data);
      $response->addCacheableDependency((new CacheableMetadata())->addCacheTags(['user_role_list']));

      return $response;
    });
    $response->addCacheableDependency($context);
    return $response;
  }

}
