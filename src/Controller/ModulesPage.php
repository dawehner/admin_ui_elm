<?php

namespace Drupal\admin_ui_elm\Controller;


use Drupal\Core\Extension\Extension;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;

class ModulesPage {

  public function view() {
    $build['#attached']['library'][] = 'admin_ui_elm/modules';
    $build['#markup'] = '<div id="root" />';
    return $build;
  }

  public function getModules() {
    $module_handler = \Drupal::moduleHandler();
    $modules = system_rebuild_module_data();
    return new JsonResponse(array_values(array_map(function (Extension $extension) use ($module_handler) {
      $enabled = $module_handler->moduleExists($extension->getName());
      $info_file = Yaml::decode(file_get_contents($extension->getPathname()));
      $help_url = $enabled && $module_handler->implementsHook('help') ? Url::fromRoute('help.page', ['name' => $extension->getName()])->toString() : NULL;
      $permission_url = $enabled ? Url::fromRoute('user.admin_permissions', [], ['fragment' => $extension->getName()])->toString() : NULL;
      $config_url = $enabled && isset($info_file['configure']) ? Url::fromRoute($info_file['configure'])->toString() : NULL;
      return [
        'name' => $extension->getName(),
        'human_name' => $info_file['name'],
        'description' => isset($info_file['description']) ? $info_file['description'] : NULL,
        'package' => isset($info_file['package']) ? $info_file['package'] : NULL,
        'dependencies' => isset($info_file['dependencies']) ? $info_file['dependencies'] : [],
        'status' => $enabled,
        'helpUrl' => $help_url,
        'permissionUrl' => $permission_url,
        'configUrl' => $config_url,
      ];
    }, $modules)));
  }

  public function enableModule($module_name) {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
    $module_installer = \Drupal::service('module_installer');
    $module_installer->install([$module_name]);
    return new JsonResponse($module_name);
  }

}
