<?php

namespace Drupal\admin_ui_elm\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RenderContext;
use Drupal\user\Entity\Role;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DblogPage extends ControllerBase {

  public function view() {
    $build['#attached']['library'][] = 'admin_ui_elm/dblog';
    $build['#markup'] = '<div id="root" />';
    return $build;
  }

}
