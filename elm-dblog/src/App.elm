module App exposing (..)

import Html exposing (Html, text, div, img, tr, td, table, th, thead, tbody, button, span, label, select, option)
import Html.Attributes exposing (src, class, value, multiple)
import Html.Events exposing (onClick, onClick, on, targetValue)
import Http
import WebSocket
import RemoteData
import Json.Decode as JD
import Json.Decode.Pipeline as JDP
import Time.DateTime
import EveryDictList
import Set
import MultiSelect


type alias Model =
    { flags :
        { baseUrl : String
        }
    , dblogs : RemoteData.WebData (List Dblog)
    , page : Int
    , sortState : Maybe TableSortState
    , filterState : TableFilterState
    , availableTypes : Set.Set String
    }


type alias TableSortState =
    { field : Field
    , order : Order
    }


type alias TableFilterState =
    EveryDictList.EveryDictList Field (Set.Set String)


type Order
    = ASC
    | DESC


type Field
    = Wid
    | User
    | Type
    | Timestamp
    | Severity


fieldToString : Field -> String
fieldToString field =
    case field of
        Wid ->
            "wid"

        User ->
            "name"

        Type ->
            "type"

        Timestamp ->
            "timestamp"

        Severity ->
            "severity"


type alias Dblog =
    { wid : Int
    , user : String
    , type_ : String
    , message : String
    , location : String
    , timestamp : Float
    }


init : String -> ( Model, Cmd Msg )
init baseUrl =
    let
        model =
            { flags =
                { baseUrl = baseUrl
                }
            , dblogs = RemoteData.Loading
            , page = 0
            , sortState = Nothing
            , filterState =
                EveryDictList.fromList
                    [ ( Type, Set.empty )
                    , ( Severity, Set.empty )
                    ]
            , availableTypes = Set.empty
            }
    in
        ( model, getDblog baseUrl 0 model.sortState model.filterState )


type Msg
    = NoOp
    | DblogLoadingResult (RemoteData.WebData (List Dblog))
    | NextPage
    | PreviousPage
    | SortTable Field Order
    | ChangeFilterState Field (List String)
    | NewWSDBlog (Result String Dblog)


remoteForceAppend : a -> RemoteData.RemoteData e (List a) -> RemoteData.RemoteData e (List a)
remoteForceAppend a remotedata =
    case remotedata of
        RemoteData.Success list ->
            RemoteData.Success (a :: list)

        RemoteData.NotAsked ->
            RemoteData.Success [ a ]

        RemoteData.Loading ->
            RemoteData.Success [ a ]

        RemoteData.Failure _ ->
            RemoteData.Success [ a ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        DblogLoadingResult result ->
            let
                types_ =
                    RemoteData.withDefault [] result
                        |> List.map .type_
                        |> List.foldr Set.insert model.availableTypes
            in
                ( { model | dblogs = result, availableTypes = types_ }, Cmd.none )

        NextPage ->
            ( { model | page = model.page + 1 }, getDblog model.flags.baseUrl (model.page + 1) model.sortState model.filterState )

        PreviousPage ->
            ( { model | page = model.page - 1 }, getDblog model.flags.baseUrl (model.page - 1) model.sortState model.filterState )

        SortTable field order ->
            let
                model_ =
                    { model | sortState = Just (TableSortState field order) }
            in
                ( model_, getDblog model.flags.baseUrl model_.page model_.sortState model_.filterState )

        ChangeFilterState field values ->
            let
                filterState =
                    model.filterState

                filterState_ =
                    EveryDictList.insert field (Set.fromList values) filterState
            in
                ( { model | filterState = filterState_ }, getDblog model.flags.baseUrl model.page model.sortState filterState_ )

        NewWSDBlog (Ok dblog) ->
            ( { model
                | dblogs = remoteForceAppend dblog model.dblogs
                , availableTypes =
                    Set.insert dblog.type_ model.availableTypes
              }
            , Cmd.none
            )

        NewWSDBlog (Err err) ->
            let
                _ =
                    Debug.log "error in websocket" err
            in
                ( model, Cmd.none )


stringFloatDecoder : JD.Decoder Float
stringFloatDecoder =
    (JD.string)
        |> JD.andThen
            (\val ->
                case String.toFloat val of
                    Ok f ->
                        JD.succeed f

                    Err e ->
                        JD.fail e
            )


stringIntDecoder : JD.Decoder Int
stringIntDecoder =
    (JD.string)
        |> JD.andThen
            (\val ->
                case String.toInt val of
                    Ok f ->
                        JD.succeed f

                    Err e ->
                        JD.fail e
            )


decodeDblogEntries : JD.Decoder (List Dblog)
decodeDblogEntries =
    JD.list decodeDblogEntry


decodeDblogEntry : JD.Decoder Dblog
decodeDblogEntry =
    JDP.decode Dblog
        |> JDP.required "wid" stringIntDecoder
        |> JDP.required "user" JD.string
        |> JDP.required "type" JD.string
        |> JDP.required "message" JD.string
        |> JDP.required "link" JD.string
        |> JDP.required "timestamp" stringFloatDecoder


getDblog : String -> Int -> Maybe TableSortState -> TableFilterState -> Cmd Msg
getDblog baseUrl page sort filters =
    let
        pageString =
            "page=" ++ toString page

        sortQuery =
            Maybe.map
                (\{ field, order } ->
                    ("sort_by="
                        ++ fieldToString field
                        ++ "_"
                        ++ (case order of
                                ASC ->
                                    "asc"

                                DESC ->
                                    "desc"
                           )
                    )
                )
                sort
                |> Maybe.withDefault
                    ""

        filtersQuery =
            EveryDictList.map
                (\key values ->
                    Set.toList values
                        |> List.map (\value -> ((fieldToString key) ++ "[]=" ++ value))
                )
                filters
                |> EveryDictList.values
                |> List.concat
                |> String.join "&"
    in
        Http.get
            (baseUrl
                ++ "admin/reports/dblog/rest?_format=json"
                ++ "&"
                ++ pageString
                ++ "&"
                ++ sortQuery
                ++ "&"
                ++ filtersQuery
            )
            decodeDblogEntries
            |> RemoteData.sendRequest
            |> Cmd.map DblogLoadingResult


view : Model -> Html Msg
view model =
    div []
        [ filterForm (Set.toList model.availableTypes) model.filterState
        , viewRemoteData (viewDblogTable model.sortState) model.dblogs
        , viewPager model.page (RemoteData.map List.length model.dblogs |> RemoteData.toMaybe |> Maybe.withDefault 0)
        ]


viewPager : Int -> Int -> Html Msg
viewPager page currentPageCount =
    let
        nextPageButton =
            button [ onClick NextPage ] [ text "Next" ]

        previousPageButton =
            button [ onClick PreviousPage ] [ text "Previous" ]
    in
        if page > 0 then
            if currentPageCount == 10 then
                span [] [ previousPageButton, nextPageButton ]
            else
                span [] [ previousPageButton ]
        else
            span [] [ nextPageButton ]


viewRemoteData : (a -> Html Msg) -> RemoteData.WebData a -> Html Msg
viewRemoteData render data =
    case data of
        RemoteData.Success a ->
            render a

        RemoteData.Loading ->
            text "loading"

        RemoteData.Failure _ ->
            text "failure"

        RemoteData.NotAsked ->
            text "not asked"


typeSelect : List String -> TableFilterState -> Html Msg
typeSelect types filterState =
    let
        defaultOptions =
            MultiSelect.defaultOptions (ChangeFilterState Type)
    in
        MultiSelect.multiSelect
            ({ defaultOptions
                | items =
                    List.map (\k -> { value = k, text = k, enabled = True }) types
             }
            )
            []
            (EveryDictList.get Type filterState |> Maybe.map Set.toList |> Maybe.withDefault [])


severitySelect : TableFilterState -> Html Msg
severitySelect filterState =
    let
        defaultOptions =
            MultiSelect.defaultOptions (ChangeFilterState Severity)
    in
        MultiSelect.multiSelect
            ({ defaultOptions
                | items =
                    [ { value = "0", text = "Emergency", enabled = True }
                    , { value = "1", text = "Alert", enabled = True }
                    , { value = "2", text = "Critical", enabled = True }
                    , { value = "3", text = "Error", enabled = True }
                    , { value = "4", text = "Warning", enabled = True }
                    , { value = "5", text = "Notice", enabled = True }
                    , { value = "6", text = "Info", enabled = True }
                    , { value = "7", text = "Debug", enabled = True }
                    ]
             }
            )
            []
            (EveryDictList.get Severity filterState |> Maybe.map Set.toList |> Maybe.withDefault [])


filterForm : List String -> TableFilterState -> Html Msg
filterForm types filterState =
    div [ class "form--inline" ]
        [ div [ class "form-item" ]
            [ label [] [ text "Type" ]
            , typeSelect types filterState
            ]
        , div [ class "form-item" ]
            [ label []
                [ text "Severity" ]
            , severitySelect filterState
            ]
        ]


viewDblogTable : Maybe TableSortState -> List Dblog -> Html Msg
viewDblogTable sort dblogs =
    table []
        [ thead []
            [ tableHeader sort Type <| text "Type"
            , tableHeader sort Timestamp <| text "Date"
            , th [] [ text "Message" ]
            , tableHeader sort User <| text "User"
            ]
        , tbody [] <| List.map viewDblogRow dblogs
        ]


viewDblogRow : Dblog -> Html Msg
viewDblogRow dblog =
    tr []
        [ td [] [ text dblog.type_ ]
        , td [] [ Time.DateTime.fromTimestamp dblog.timestamp |> Time.DateTime.toISO8601 |> text ]
        , td [] [ limitString 100 dblog.message |> text ]
        , td [] [ text dblog.user ]
        ]


tableHeader : Maybe TableSortState -> Field -> Html Msg -> Html Msg
tableHeader sort field_ header =
    case sort of
        Nothing ->
            unsortedTableHeader (SortTable field_ ASC) header

        Just { field, order } ->
            if field_ == field then
                sortedTableHeader (SortTable field_ (reverseOrder order)) order header
            else
                unsortedTableHeader (SortTable field_ ASC) header


reverseOrder : Order -> Order
reverseOrder order =
    case order of
        ASC ->
            DESC

        DESC ->
            ASC


unsortedTableHeader : Msg -> Html Msg -> Html Msg
unsortedTableHeader msg header =
    th [ onClick msg ]
        [ header
        ]


sortedTableHeader : Msg -> Order -> Html Msg -> Html Msg
sortedTableHeader msg order header =
    th [ class "is-active", onClick msg ]
        [ header
        , span
            [ class
                (if order == ASC then
                    "tablesort tablesort--desc"
                 else
                    "tablesort tablesort--asc"
                )
            ]
            []
        ]


limitString : Int -> String -> String
limitString length string =
    if String.length string > length then
        String.slice 0 length string
    else
        string


subscriptions : Model -> Sub Msg
subscriptions model =
    WebSocket.listen
        "ws://localhost:8081/test"
        (Debug.log "hey" >> JD.decodeString decodeDblogEntry >> NewWSDBlog)


main : Program String Model Msg
main =
    Html.programWithFlags
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
