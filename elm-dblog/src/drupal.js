(function (Drupal, drupalSettings) {
  Drupal.behaviors.elmDblog = {
    attach: function (context) {
      const root = document.getElementById('root');

      Elm.App.embed(root, drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix);
    }
  };
})(Drupal, drupalSettings);
