<?php

use Ratchet\MessageComponentInterface;
use React\Promise\Promise;
use React\Socket\ConnectionInterface;

require_once __DIR__ . '/vendor/autoload.php';

// Provide some good error handling.
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PlainTextHandler());
$whoops->register();

$loop = React\EventLoop\Factory::create();

// Actual logic of the websocket server. This is 
// what all the clients connect to.
class MyServer implements MessageComponentInterface {

  public function __construct() {
      $this->clients = [];
  }

  public function onOpen(\Ratchet\ConnectionInterface $conn) {
    print_r("new connection");
    $this->clients[] = $conn;
  }

  public function onClose(\Ratchet\ConnectionInterface $conn) {
  }

  public function onError(\Ratchet\ConnectionInterface $conn, \Exception $e) {
      $conn->close();
  }

  function onMessage(\Ratchet\ConnectionInterface $from, $msg) {
  }

  public function sendMessages($message) {
    /** @var \Ratchet\WebSocket\WsConnection $client */
    print_r($message);
    foreach ($this->clients as $client) {
      print_r('client ...');
      $client->send($message);
    }
  }

}

// Setup code for websocket server.
$app = new Ratchet\App('localhost', 8081, '127.0.0.1', $loop);
$my_server = new MyServer();
$app->route('/test', $my_server, array('*'));
$app->route('/echo', new Ratchet\Server\EchoServer, array('*'));

// HTTP server in react which calls sendMessages on our websocket broker,
// once the HTTP request is fully transfered.
$server = new \React\Http\Server(function (Psr\Http\Message\ServerRequestInterface $request) use (&$my_server) {
  return new Promise(function ($resolve, $reject) use ($request, &$my_server) {
    $content = '';
    $request->getBody()->on('data', function ($data) use (&$content) {
      $content .= $data;
    });
    $request->getBody()->on('end', function () use ($resolve, &$content, &$my_server) {
      $response = new React\Http\Response(
        200,
        array('Content-Type' => 'text/plain'),
        "Hello World!\n"
      );
      $resolve($response);

      $my_server->sendMessages($content);
    });
  });
});


$socket = new React\Socket\Server(8080, $loop);
$server->listen($socket);

echo "Server running at http://127.0.0.1:8080\n";
$loop->run();

echo "Server running at http://127.0.0.1:8081\n";
$app->run();

