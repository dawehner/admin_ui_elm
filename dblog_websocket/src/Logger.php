<?php

namespace Drupal\dblog_websocket;

require_once __DIR__ . '/../vendor/autoload.php';

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;

class Logger implements LoggerInterface {

  use RfcLoggerTrait;
  use DependencySerializationTrait;


  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    \Drupal::httpClient()->post('http://127.0.0.1:8080', [
      'json' => [
        'wid' => (string) 123,
        'user' => User::load($context['uid'])->getDisplayName(),
        'type' => Unicode::substr($context['channel'], 0, 64),
        'message' => strtr($message, $context),
        'severity' => $level,
        'link' => $context['link'],
        'location' => $context['request_uri'],
        'referer' => $context['referer'],
        'hostname' => Unicode::substr($context['ip'], 0, 128),
        'timestamp' => (string) $context['timestamp'],
      ]
    ]);
  }

}
