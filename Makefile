build-permissions:
	cd elm-permissions && elm-make src/App.elm --output=App.js

dev-permissions:
	cd elm-permissions && elm-app start

build-modules:
	cd elm-modules && elm-make src/App.elm --output=App.js

dev-modules:
	cd elm-modules && elm-app start

build-dblog:
	cd elm-dblog && elm-make src/App.elm --output=App.js

dev-dblog:
	cd elm-dblog && elm-app start

build : build-permissions build-modules build-dblog
